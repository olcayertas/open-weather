//
// Created by Olcay Ertaş on 2.06.2020.
// Copyright (c) 2020 Porte. All rights reserved.
//

import Foundation
import Combine
import CoreLocation


class ForecastViewModel: NSObject, ObservableObject {

    private let apiClient = ApiClient()
    private var cancelBag = Set<AnyCancellable>()
    private var locationManager = CLLocationManager()
    private var lastLocation: CLLocation?
    private var locationAuthorizationStatus = CLAuthorizationStatus.notDetermined

    @Published var isLoading = false {
        willSet {
            self.objectWillChange.send()
        }
    }

    @Published var city = "" {
        willSet {
            self.objectWillChange.send()
        }
        didSet {
            query = ""
            predictions = []
            getForecastFor(city: city)
        }
    }

    @Published var query = "" {
        willSet {
            self.objectWillChange.send()
        }
        didSet {
            if query.count == 0 {
                predictions = []
            }
            getCities(query: query)
        }
    }

    @Published var hasLocationAuthorization = false {
        willSet {
            self.objectWillChange.send()
        }
    }

    @Published var weatherForecast: ForecastModel? {
        willSet {
            self.objectWillChange.send()
        }
    }

    @Published var sevenDayForecast: SevenDayForecast? {
        willSet {
            self.objectWillChange.send()
        }
    }

    @Published var daily: [DailyForecast] = [] {
        willSet {
            self.objectWillChange.send()
        }
    }

    @Published var cityDetail: CityDetail? {
        willSet {
            self.objectWillChange.send()
        }
    }

    @Published var predictions: [Prediction] = [] {
        willSet {
            self.objectWillChange.send()
        }
    }

    @Published var error: Error? {
        willSet {
            self.objectWillChange.send()
        }
    }

    @Published var weatherIcon: Data = Data() {
        willSet {
            self.objectWillChange.send()
        }
    }

    override init() {
        super.init()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
    }

    public func getForecast(query: String? = nil) {
        isLoading = true
        if hasLocationAuthorization, let location = lastLocation {
            getForecastForLocation(location)
        } else {
            requestLocationPermissionIfNeeded()
        }
    }

    func getSevenDayForecast() {
        guard let forecast = weatherForecast else {
            return
        }
        guard let lat = forecast.coord?.lat, let lng = forecast.coord?.lon else {
            return
        }
        apiClient.getSevenDayForecastForLocation(lat: lat, lng: lng)?.sink(receiveCompletion: { [weak self] completion in
            self?.isLoading = false
            switch completion {
            case .finished:
                print("ForecastViewModel: getSevenDayForecastForLocation: finished")
            case .failure(let error):
                self?.error = error
                print("ForecastViewModel: getSevenDayForecastForLocation: \(error.localizedDescription)")
            }
        }, receiveValue: { [weak self] result in
            self?.sevenDayForecast = result
            self?.daily = result.daily ?? []
            self?.getIcons()
            print("ForecastViewModel: getSevenDayForecastForLocation: received cities")
        }).store(in: &cancelBag)
    }

    public func getCities(query: String? = nil) {
        if let query = query {
            apiClient.getCitiesFor(query: query)?.sink(receiveCompletion: { [weak self] completion in
                self?.isLoading = false
                switch completion {
                case .finished:
                    print("ForecastViewModel: getCities: finished")
                case .failure(let error):
                    self?.error = error
                    print("ForecastViewModel: getCities: \(error.localizedDescription)")
                }
            }, receiveValue: { [weak self] result in
                self?.predictions = result.predictions
                //self?.predictions.prettyPrint()
                print("ForecastViewModel: getCities: received cities")
            }).store(in: &cancelBag)
        }
    }

    public func getWeatherIcon() {
        guard let icon = weatherForecast?.weather?.first?.icon else {
            print("ForecastViewModel: getWeatherIcon: Failed to get icon name!")
            return
        }
        isLoading = true
        getWeatherIcon(icon: icon) { [weak self] data in
            self?.weatherIcon = data
        }
    }

    public func getWeatherIcon(icon: String, completion: @escaping (Data) -> Void) {
        isLoading = true
        apiClient.getWeatherImage(icon: icon)?.sink(receiveCompletion: { [weak self] completion in
            self?.isLoading = false
            switch completion {
            case .finished:
                print("ForecastViewModel: getWeatherIcon: finished")
            case .failure(let error):
                self?.error = error
                print("ForecastViewModel: getWeatherIcon: \(error.localizedDescription)")
            }
        }) { data in
            completion(data)
        }.store(in: &cancelBag)
    }

    public func getForecastForLocation(_ location: CLLocation) {
        let lat = location.coordinate.latitude
        let lng = location.coordinate.longitude
        getForecastForLocation(lat: lat, lng: lng)
    }

    public func getForecastForLocation(lat: Double, lng: Double) {
        print("ForecastViewModel: getForecast: getting forecast...")
        isLoading = true
        apiClient.getForecastForLocation(lat: lat, lng: lng)?.sink(receiveCompletion: { [weak self] completion in
            self?.isLoading = false
            switch completion {
            case .finished:
                print("ForecastViewModel: getForecast: finished")
            case .failure(let error):
                self?.error = error
                print("ForecastViewModel: getForecast: \(error.localizedDescription)")
            }
        }, receiveValue: { [weak self] forecast in
            self?.weatherForecast = forecast
            self?.getWeatherIcon()
            self?.getSevenDayForecast()
            print("ForecastViewModel: getForecast: received forecast")
        }).store(in: &cancelBag)
    }

    public func getForecastFor(city: String) {
        print("ForecastViewModel: getForecastForCity: getting forecast...")
        isLoading = true
        apiClient.getForecastFor(city: city)?.sink(receiveCompletion: { [weak self] completion in
            self?.isLoading = false
            switch completion {
            case .finished:
                print("ForecastViewModel: getForecastForCity: finished")
            case .failure(let error):
                self?.error = error
                print("ForecastViewModel: getForecastForCity: \(error.localizedDescription)")
            }
        }, receiveValue: { [weak self] forecast in
            forecast.prettyPrint()
            self?.weatherForecast = forecast
            self?.getWeatherIcon()
            self?.getSevenDayForecast()
            print("ForecastViewModel: getForecastForCity: received forecast")
        }).store(in: &cancelBag)
    }

    private func getIcons() {
        sevenDayForecast?.daily?.forEach { forecast in
            guard let icon = forecast.weather?.first?.icon else {
                print("ForecastViewModel: getWeatherIcon: Failed to get icon name!")
                return
            }
            getWeatherIcon(icon: icon) { data in
                forecast.imageData = data
            }
        }
    }

    public func requestLocationPermissionIfNeeded() {
        locationManager.requestWhenInUseAuthorization()
    }
}

extension ForecastViewModel: CLLocationManagerDelegate {

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        lastLocation = locations.last
    }

    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        locationAuthorizationStatus = status
        hasLocationAuthorization = (status == .authorizedWhenInUse)
        switch status {
        case .notDetermined:
            print("ForecastViewModel: Location usage not determined!")
        case .restricted:
            print("ForecastViewModel: Location usage restricted!")
        case .denied:
            print("ForecastViewModel: Location usage denied!")
        case .authorizedAlways:
            print("ForecastViewModel: Location usage allowed always!")
            manager.startUpdatingLocation()
        case .authorizedWhenInUse:
            print("ForecastViewModel: Location usage allowed in use!")
            manager.requestLocation()
        @unknown default:
            print("ForecastViewModel: Location usage default!")
        }
    }

    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("ForecastViewModel: \(error.localizedDescription)")
    }
}
