//
// Created by Olcay Ertaş on 2.06.2020.
// Copyright (c) 2020 Porte. All rights reserved.
//

import Foundation

struct CoordinateModel: Codable {
    var lon: Double?
    var lat: Double?
}

struct WeatherModel: Codable {
    var id: Int?
    var main: String?
    var description: String?
    var icon: String?
}

struct MainModel: Codable {
    var temp: Double?
    var pressure: Double?
    var humidity: Int?
    var temp_min: Double?
    var temp_max: Double?
    var sea_level: Double?
    var grnd_level: Double?
}

struct WindModel: Codable {
    var speed: Double?
    var deg: Int?
}

struct CloudsModel: Codable {
    var all: Int?
}

struct SysModel: Codable {
    var message: String?
    var country: String?
    var sunrise: Int?
    var sunset: Int?
}

struct ForecastModel: Codable {
    var coord: CoordinateModel?
    var weather: [WeatherModel]?
    var base: String?
    var main: MainModel?
    var wind: WindModel?
    var clouds:CloudsModel?
    var dt: Int?
    var sys: SysModel?
    var id: Int?
    var name: String?
    var cod: Int?
}
