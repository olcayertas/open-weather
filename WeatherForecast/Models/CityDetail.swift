//
// Created by Olcay Ertaş on 6.06.2020.
// Copyright (c) 2020 Porte. All rights reserved.
//

import Foundation

struct Location: Codable {
    var lat: Double?
    var lng: Double?
}

struct Geometry: Codable {
    var location: Location?
}

struct Result: Codable {
    var geometry: Geometry?
}

struct CityDetail: Codable {
    var result: Result?
}
