//
// Created by Olcay Ertaş on 6.06.2020.
// Copyright (c) 2020 Porte. All rights reserved.
//

import Foundation
import SwiftUI

struct Temp: Codable {
    var day: Double?
    var min: Double?
    var max: Double?
    var night: Double?
    var eve: Double?
    var morn: Double?
}

struct FeelsLike: Codable {
    var day: Double?
    var night: Double?
    var eve: Double?
    var morn: Double?
}

class DailyForecast: NSObject, Codable, ObservableObject {
    var weather: [WeatherModel]?
    var dt: Double?
    var pressure: Int?
    var humidity: Int?
    var temp: Temp?
    var feels_like: FeelsLike?
    var imageData: Data? {
        willSet {
            self.objectWillChange.send()
        }
    }
}

class SevenDayForecast: Codable {
    var daily: [DailyForecast]?
}
