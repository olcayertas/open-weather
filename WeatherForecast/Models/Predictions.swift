//
// Created by Olcay Ertaş on 5.06.2020.
// Copyright (c) 2020 Porte. All rights reserved.
//

import Foundation

struct Prediction: Codable {
    var id: String?
    var place_id: String?
    var description: String?
}

struct Predictions: Codable {
    var predictions: [Prediction]
    var code: String?
    var message: String?
}
