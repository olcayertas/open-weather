//
// Created by Olcay Ertaş on 6.06.2020.
// Copyright (c) 2020 Porte. All rights reserved.
//

import SwiftUI
import CoreLocation
import Combine


struct SevenDayForeCastView: View {

    @Binding var forecasts: [DailyForecast]

    var body: some View {
        ForEach(forecasts, id: \.dt) { (forecast: DailyForecast) in
            VStack {
                HStack {
                    Text(String.dayNameFrom(utc: forecast.dt))
                    Spacer()
                    HStack(alignment: .bottom, spacing: 32) {
                        Image(uiImage: UIImage(data: forecast.imageData ?? Data()) ?? UIImage())
                                .resizable()
                                .aspectRatio(contentMode: .fit)
                                .frame(width: 64, height: 32, alignment: .center)
                        Text("\(Int(forecast.temp?.max ?? 0))°").frame(minWidth: 40)
                        Text("\(Int(forecast.temp?.min ?? 0))°").frame(minWidth: 40)
                    }
                }.padding(.top, 16)
                Divider()
            }
        }
    }
}
