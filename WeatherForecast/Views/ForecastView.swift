//
//  ForecastView.swift
//  WeatherForecast
//
//  Created by Olcay Ertaş on 2.06.2020.
//  Copyright © 2020 Porte. All rights reserved.
//

import SwiftUI
import CoreLocation
import Combine


struct ForecastView: View {

    @State private var searchText: String = ""
    @State private var selected: String = ""
    @State private var show = false
    @ObservedObject var viewModel = ForecastViewModel()

    var body: some View {
        LoadingView(isShowing: $viewModel.isLoading) {
            ScrollView(.vertical, showsIndicators: false) {
                VStack(alignment: self.viewModel.predictions.count > 0 ? .leading : .center, spacing: 0) {
                    TextField("Search", text: self.$viewModel.query).textFieldStyle(RoundedBorderTextFieldStyle())
                    if self.viewModel.hasLocationAuthorization == false {
                        Text("Please give location access to get forecast for your location or search a city")
                    }
                    if self.viewModel.predictions.count > 0 {
                        CityListView(
                                cityList: self.$viewModel.predictions,
                                selectedCity: self.$viewModel.city
                        ).padding(.top, 8)
                    } else {
                        CurrentForecastView(
                                forecast: self.$viewModel.weatherForecast,
                                icon: self.$viewModel.weatherIcon
                        ).padding(.vertical, 32)
                        SevenDayForeCastView(forecasts: self.$viewModel.daily)
                    }
                }.padding(.horizontal, 16).padding(.top, 16)
            }.navigationBarTitle("Weather App", displayMode: .inline).onAppear {
                self.viewModel.getForecast()
            }
        }
    }
}

struct ForecastView_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            ForecastView()
                    .previewDevice(PreviewDevice(rawValue: "iPhone 11 Pro Max"))
                    .previewDisplayName("iPhone 11 Pro Max")
            //.previewLayout(.sizeThatFits)
            //.environment(\.colorScheme, ColorScheme.dark)
        }
    }
}
