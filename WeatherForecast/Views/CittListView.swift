//
// Created by Olcay Ertaş on 6.06.2020.
// Copyright (c) 2020 Porte. All rights reserved.
//

import SwiftUI

struct CityListView: View {

    @Binding var cityList: [Prediction]
    @Binding var selectedCity: String

    var body: some View {
        VStack(alignment: .leading, spacing: 8) {
            ForEach(self.cityList, id: \.id) { (prediction: Prediction) in
                Button<Text>(action: {
                    if let sub = prediction.description?.split(separator: ",").first {
                        self.selectedCity = String(sub)
                    }
                }, label: {
                    Text(prediction.description ?? "").font(.body).foregroundColor(.black)
                })
            }
        }
    }
}