//
//  ContentView.swift
//  WeatherForecast
//
//  Created by Olcay Ertaş on 2.06.2020.
//  Copyright © 2020 Porte. All rights reserved.
//

import SwiftUI

public class ApiKey: ObservableObject {
    @Published var value: String = "55820ee183e0543f456338851f6e92b9" {
        didSet {
            ApiKeyManager.setApiKey(key: value)
        }
    }
}

public struct APIKeyView: View {

    @ObservedObject private var apiKey = ApiKey()

    public var body: some View {
        NavigationView {
            ScrollView(.vertical, showsIndicators: false) {
                HStack(alignment: .center, spacing: 16) {
                    VStack(alignment: .center, spacing: 16) {
                        Text("Weather App").padding(.bottom, 32)
                                .font(.title)
                                .foregroundColor(.gray)
                        TextField("Enter API key", text: $apiKey.value)
                                .textFieldStyle(RoundedBorderTextFieldStyle())
                                .multilineTextAlignment(TextAlignment.center)
                        NavigationLink(destination: ForecastView()) {
                            Text("CONTINUE")
                        }.disabled(apiKey.value.count < 5)
                    }
                }.padding(32)
            }
        }.onAppear {
            ApiKeyManager.setApiKey(key: self.apiKey.value)
        }
    }
}

struct APIKeyView_Previews: PreviewProvider {

    static var previews: some View {
        Group {
            APIKeyView()
                .previewDevice(PreviewDevice(rawValue: "iPhone 11 Pro Max"))
                .previewDisplayName("iPhone 11 Pro Max")
                //.previewLayout(.sizeThatFits)
                //.environment(\.colorScheme, ColorScheme.dark)
        }
    }
}
