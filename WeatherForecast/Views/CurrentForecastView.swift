//
// Created by Olcay Ertaş on 6.06.2020.
// Copyright (c) 2020 Porte. All rights reserved.
//

import SwiftUI
import CoreLocation
import Combine


struct CurrentForecastView: View {

    @Binding var forecast: ForecastModel?
    @Binding var icon: Data

    var body: some View {
        VStack(alignment: .center, spacing: 0) {
            Text(forecast?.name ?? "").font(.title).foregroundColor(.gray)
            Image(uiImage: UIImage(data: icon) ?? UIImage()).frame(width: 80, height: 80, alignment: .center)
            Text("\(Int((forecast?.main?.temp ?? 0)))°").font(.title).foregroundColor(.black)
        }
    }
}

