//
//  Extensions.swift
//  WeatherForecast
//
//  Created by Olcay Ertaş on 3.06.2020.
//  Copyright © 2020 Porte. All rights reserved.
//

import Foundation

public class PrettyEncoder: JSONEncoder {

    override init() {
        super.init()
        outputFormatting = .prettyPrinted
    }
}

extension Data {
    func prettyPrint() { /// NSString gives us a nice sanitized debugDescription
        guard let object = try? JSONSerialization.jsonObject(with: self, options: []),
              let data = try? JSONSerialization.data(withJSONObject: object, options: [.prettyPrinted]),
              let prettyPrintedString = NSString(data: data, encoding: String.Encoding.utf8.rawValue) else {
            return
        }
        print(prettyPrintedString)
    }
}

extension Encodable {

    func prettyPrint() {
        let prettyData = try? PrettyEncoder().encode(self)
        let prettyStr = String(data: prettyData ?? Data(), encoding: .utf8)
        print(prettyStr ?? "")
    }
}

extension String {

    static func dayNameFrom(utc: Double?) -> String {
        let date = Date(timeIntervalSince1970: utc ?? 0)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE"
        dateFormatter.timeZone = .current
        return dateFormatter.string(from: date)
    }
}
