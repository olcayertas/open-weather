//
// Created by Olcay Ertaş on 2.06.2020.
// Copyright (c) 2020 Porte. All rights reserved.
//

import Foundation
import Combine

public enum ApiClientError: Error, LocalizedError {

    case failedToGetBaseUrl
    case failedToCreateUrl(String)
    case failedToDecodeResponse
    case networkError(URLError)
    case apiError(String)
    case invalidServerResponse
    case unauthorized
    case unknown

    public var errorDescription: String? {
        switch self {
        case .unknown:
            return "Unknown error"
        case .apiError(let error):
            return error
        case .failedToGetBaseUrl:
            return "Did you set base URL?"
        case .failedToCreateUrl(let endpoint):
            return "Invalid URL: \(endpoint)"
        case .failedToDecodeResponse:
            return "Failed to decode response!"
        case .invalidServerResponse:
            return "Invalid server response!"
        case .networkError(let error):
            return error.localizedDescription
        case .unauthorized:
            return "Unauthorized!"
        }
    }
}

struct ApiClient {

    private static let _mapsKey = "AIzaSyD06IIB45kRjPu1y0ZkUoGxDD4uK38onbY"
    private static let _mapsAutoCompleteBaseUrl = "https://maps.googleapis.com/maps/api/place/autocomplete/json"
    private static let _mapsDetailBaseUrl = "https://maps.googleapis.com/maps/api/place/details/json"
    private let _mapsAutoCompleteUrlWitKey = "\(_mapsAutoCompleteBaseUrl)?key=\(_mapsKey)&types=(cities)"
    private let _mapsDetailUrlWitKey = "\(_mapsDetailBaseUrl)?key=\(_mapsKey)&fields=geometry"
    private let _baseUrl = "https://api.openweathermap.org/data/2.5"

    private var _weatherUrl: String {
        "/weather"
    }

    private var _oneCall: String {
        "/onecall"
    }

    private func baseUrl(_ path: String) -> String {
        let base = "\(_baseUrl)\(path)?units=metric"
        guard let lang = Locale.current.languageCode else {
            return base
        }
        guard ApiKeyManager.apiKey.count > 0 else {
            print("APiClient: Failed to get api key!")
            return "\(base)&lang=\(lang)"
        }
        return "\(base)&lang=\(lang)&appid=\(ApiKeyManager.apiKey)"
    }

    func getCitiesFor(query: String) -> AnyPublisher<Predictions, Error>? {
        guard query.count > 2 else {
            return nil
        }
        return fetchAndDecode(urlString: "\(_mapsAutoCompleteUrlWitKey)&input=\(query.lowercased())", type: Predictions.self)
    }

    func getForecastForLocation(lat: Double, lng: Double) -> AnyPublisher<ForecastModel, Error>? {
        fetchAndDecode(urlString: "\(baseUrl(_weatherUrl))&lat=\(lat)&lon=\(lng)", type: ForecastModel.self)
    }

    func getSevenDayForecastForLocation(lat: Double, lng: Double) -> AnyPublisher<SevenDayForecast, Error>? {
        fetchAndDecode(urlString: "\(baseUrl(_oneCall))&lat=\(lat)&lon=\(lng)&exclude=hourly", type: SevenDayForecast.self)
    }

    func getForecastFor(city: String) -> AnyPublisher<ForecastModel, Error>? {
        guard city.count > 3 else {
            return nil
        }
        return fetchAndDecode(urlString: "\(baseUrl(_weatherUrl))&q=\(city)", type: ForecastModel.self)
    }
    
    func getWeatherImage(icon: String) -> AnyPublisher<Data, Error>? {
        guard let url = URL(string: "https://openweathermap.org/img/wn/\(icon)@2x.png") else {
            print("ApiClient: getWeatherImage: Failed to create URL!")
            return nil
        }
        return fetch(url: url).receive(on: RunLoop.main).eraseToAnyPublisher()
    }

    private func fetchAndDecode<T: Codable>(urlString: String, type: T.Type) -> AnyPublisher<T, Error>? {
        let set = CharacterSet()
                .union(.urlQueryAllowed)
                .union(.urlFragmentAllowed)
                .union(.urlHostAllowed)
                .union(.urlPathAllowed)
        guard let escaped = urlString.addingPercentEncoding(withAllowedCharacters: set) else {
            return nil
        }
        guard let url = URL(string: escaped) else {
            print("ApiClient: getCities: Failed to create URL!")
            return nil
        }
        return fetch(url: url)
            .decode(type: T.self, decoder: JSONDecoder())
            .receive(on: RunLoop.main)
            .eraseToAnyPublisher()
    }

    func fetch(url: URL) -> AnyPublisher<Data, Error> {
        print("ApiClient: \(url.absoluteString)")
        return URLSession.shared.dataTaskPublisher(for: URLRequest(url: url)).tryMap { data, response -> Data in
            guard let httpResponse = response as? HTTPURLResponse else {
                print("ApiClient: Failed to get response!")
                throw ApiClientError.invalidServerResponse
            }
            if httpResponse.statusCode == 401 {
                print("ApiClient: response code: \(httpResponse.statusCode)")
                throw ApiClientError.unauthorized
            }
            //data.prettyPrint()
            return data
        }.eraseToAnyPublisher()
    }
}
