//
// Created by Olcay Ertaş on 2.06.2020.
// Copyright (c) 2020 Porte. All rights reserved.
//

import Foundation

struct ApiKeyManager {

    public static var apiKey: String {
        return UserDefaults.standard.string(forKey: "_apiKey") ?? ""
    }

    public static func setApiKey(key: String) {
        UserDefaults.standard.setValue(key, forKey: "_apiKey")
        UserDefaults.standard.synchronize()
    }
    
    public static func removeApiKey() {
        UserDefaults.standard.removeObject(forKey: "_apiKey")
        UserDefaults.standard.synchronize()
    }
}
