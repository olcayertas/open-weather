//
//  ContentView.swift
//  WeatherForecast
//
//  Created by Olcay Ertaş on 2.06.2020.
//  Copyright © 2020 Lyrebird. All rights reserved.
//

import SwiftUI

public struct APIKeyView: View {

    @State private var apiKey: String = ""

    public var body: some View {
        ScrollView(.vertical, showsIndicators: false) {
            HStack(alignment: .center, spacing: 16) {
                VStack(alignment: .center, spacing: 16) {
                    Text("Weather App").padding(.vertical, 32)
                            .font(.title).foregroundColor(.gray)
                    TextField("Enter API key", text: $apiKey)
                            .textFieldStyle(RoundedBorderTextFieldStyle())
                            .multilineTextAlignment(TextAlignment.center)
                    Button<Text>(action: {
                        print("pressed")
                    }, label: {
                        Text("CONTINUE")
                    })
                }
            }.padding(32)
        }
    }
}

struct ContentView_Previews: PreviewProvider {

    static var previews: some View {
        Group {
            APIKeyView()
                .previewDevice(PreviewDevice(rawValue: "iPhone 11 Pro Max"))
                .previewDisplayName("iPhone 11 Pro Max")
                //.previewLayout(.sizeThatFits)
                .environment(\.colorScheme, ColorScheme.dark)
        }
    }
}
